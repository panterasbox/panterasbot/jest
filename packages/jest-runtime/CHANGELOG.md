# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [27.3.0](https://github.com/facebook/jest/compare/v27.2.5...v27.3.0) (2021-10-17)


### Bug Fixes

* added module loader event emitters ([6bb1724](https://github.com/facebook/jest/commit/6bb1724722900356ae8fedbcbc50b17b7ba9f3fe))
* correct `instanceof` for `ModernFakeTimers` and `LegacyFakeTimers` methods ([#11946](https://github.com/facebook/jest/issues/11946)) ([46c9c13](https://github.com/facebook/jest/commit/46c9c13811ca20a887e2826c03852f2ccdebda7a))
