# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [27.3.0](https://github.com/facebook/jest/compare/v27.2.5...v27.3.0) (2021-10-17)


### Features

* **environment-jsdom:** allow passing html content to jsdom environment ([#11950](https://github.com/facebook/jest/issues/11950)) ([7f39f0a](https://github.com/facebook/jest/commit/7f39f0a589703191a0cd3c6db558920281f7195f))
