# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# [27.3.0](https://github.com/facebook/jest/compare/v27.2.5...v27.3.0) (2021-10-17)

**Note:** Version bump only for package jest-message-util
